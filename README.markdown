# Functional Programming Course

See https://github.com/data61/fp-course for upstream.

### Written by Tony Morris & Mark Hibberd for Data61 (formerly NICTA)

### With contributions from individuals (thanks!)

#### Introduction

The course is structured according to a linear progression and uses the Haskell
programming language to learn programming concepts pertaining to functional
programming.

Exercises are annotated with a comment containing the word "Exercise." The existing code compiles, however answers have
been replaced with a call to the Haskell `error` function and so the code will throw an exception if it is run. Some
exercises contain tips, which are annotated with a preceding "Tip:". It is not necessary to adhere to tips. Tips are
provided for potential guidance, which may be discarded if you prefer a different path to a solution.

The exercises are designed in a way that requires personal guidance, so if you
attempt it on your own and feel a little lost, this is normal. All the
instructions are not contained herein.

### Getting Started

Get Stack: https://docs.haskellstack.org/en/stable/README/ (or, if you use Nix, you know what to do).

Initial setup:

```sh
git clone https://gitlab.com/wldhx/fp-course
cd fp-course
stack init
stack build course:lib
stack test course:tasty
chmod go-w . .ghci
```
After all that, launch REPL with

```sh
stack repl course:lib
:module -Course.MonadTutorial # FIXME that's something for me to fix!
```

and you're ready to hack!

#### ghcid

On-save typecheck.

```sh
stack install ghcid
ghcid --command "stack repl course:lib" # launch
```

### Tips after having started

1. Some questions take a particular form. These are called *WTF questions*. WTF
   questions are those of this form or similar:
  * What does ____ mean?
  * What does the ____ function mean?
  * What is a ____ ?
  * Where did ____ come from ?
  * What is the structure of ____ ?

  They are all answerable with the `:info` command. For example, suppose you
  have the question, "What does the `swiggletwoop` function mean?" You may
  answer this at GHCi with:

  `> :info swiggletwoop`

  You may also use `:i` for short.

2. Functional Programming techniques rely heavily on types. This reliance may
   feel foreign at first, however, it is an important part of this course. If
   you wish to know the type of an expression or value, use `:type`. For
   example,

   `> :type reverse`

   `List t -> List t`

   This tells you that the `reverse` function takes a list of elements of some
   arbitrary type (`t`) and returns a list of elements of that same type. Try
   it.

   You may also use `:t` for short.

3. GHCi has TAB-completion. For example you might type the following:

   `> :type rev`

   Now hit the TAB key. If there is only one function in scope that begins with
   the characters `rev`, then that name will auto-complete. Try it. This
   completion is context-sensitive. For example, it doesn't make sense to ask
   for the type of a data type itself, so data type names will not auto-complete
   in that context, however, if you ask for `:info`, then they are included in
   that context. Be aware of this when you use auto-complete.

   This also works for file names:

   `> readFile "/etc/pas"`

   Now hit the TAB key. If there is only one existing filename on a path that
   begins with `/etc/pas`, then that name will auto-complete. Try it.

   If there is more than one identifier that can complete, hit TAB twice
   quickly. This will present you with your options to complete.

4. Follow the types.

   You may find yourself in a position of being unsure how to proceed for a
   given exercise. You are encouraged to adopt a different perspective. Instead
   of asking how to proceed, ask how you might proceed while adhering to the
   guideline provided by the types for the exercise at hand.

   It is possible to follow the types without achieving the desired goal,
   however, this is reasonably unlikely at the start. As you become more reliant
   on following the types, you will develop more trust in the potential paths
   that they can take you, including identification of false paths.

   Your instructor must guide you where types fall short, but you should also
   take the first step. Do it.

5. Do not use tab characters

   Set up your text editor to use space characters rather than tabs.
   Using tab characters in Haskell can lead to confusing error messages.
   GHC will give you a warning if your program contains a tab character.

### Running the tests

Tests are available as a [tasty](https://hackage.haskell.org/package/tasty)
test suite.

#### tasty

Tasty tests are stored under the `test/` directory. Each module from the course
that has tests has a corresponding `<MODULE>Test.hs` file. Within each test
module, tests for each function are grouped using the `testGroup` function.
Within each test group there are test cases (`testCase` function), and
properties (`testProperty` function).

Before running the tests, ensure that you have an up-to-date installation
of GHC and cabal-install from your system package manager or use the minimal
installers found at [haskell.org](https://www.haskell.org/downloads#minimal).

To run the full test suite, build the project as follows:

```sh
stack test course:tasty
```

Tasty will also allow you to run only those tests whose description match a
pattern. Tests are organised in nested groups named after the relevant module
and function, so pattern matching should be intuitive. For example, to run the
tests for the `List` module you could run:

    > stack test course:tasty --ta "-p Tests.List."

Likewise, to run only the tests for the `headOr` function in the `List` module, you could use:

    > stack test course:tasty --ta "-p List.headOr"

In addition, GHCi may be used to run tasty tests. Assuming you have run `ghci`
from the root of the project, you may do the following. Remember that GHCi has
tab completion, so you can save yourself some typing.

    > -- import the defaultMain function from Tasty - runs something of type TestTree
    > import Test.Tasty (defaultMain)
    >
    > -- Load the test module you'd like to run tests for
    > :l test/Course/ListTest.hs
    >
    > -- Browse the contents of the loaded module - anything of type TestTree
    > -- may be run
    > :browse Course.ListTest
    >
    > -- Run test for a particular function
    > defaultMain headOrTest


#### doctest

The doctest tests are a mirror of the tasty tests that reside in comments
alongside the code. They are not executable, but examples can be copied into
GHCI. Examples begin with `>>>` while properties begin with `prop>`.

### Progression

It is recommended to perform some exercises before others. The first step is to
inspect the introduction modules.

* `Course.ExactlyOne`
* `Course.Validation`

They contain examples of data structures and Haskell syntax. They do not contain
exercises and exist to provide a cursory examination of Haskell syntax. The next
step is to complete the exercises in `Course.Optional`.

After this, the following progression of modules is recommended:

* `Course.List`
* `Course.Functor`
* `Course.Applicative`
* `Course.Monad`
* `Course.FileIO`
* `Course.State`
* `Course.StateT`
* `Course.Extend`
* `Course.Comonad`
* `Course.Compose`
* `Course.Traversable`
* `Course.ListZipper`
* `Course.Parser` *(see also `Course.Person` for the parsing rules)*
* `Course.MoreParser`
* `Course.JsonParser`
* `Course.Interactive`
* `Course.Anagrams`
* `Course.FastAnagrams`
* `Course.Cheque`

During this progression, it is often the case that some exercises are abandoned
due to time constraints and the benefit of completing some exercises over
others. For example, in the progression, `Course.Functor` to `Course.Monad`, the
exercises repeat a similar theme. Instead, a participant may wish to do
different exercises, such as `Course.Parser`. In this case, the remaining
answers are filled out, so that progress on to `Course.Parser` can begin
(which depends on correct answers up to `Course.Monad`). It is recommended to
take this deviation if it is felt that there is more reward in doing so.

Answers for the exercises can be found here:
[https://github.com/tonymorris/fp-course](https://github.com/tonymorris/fp-course)

After these are completed, complete the exercises in the `projects` directory.

### Introducing Haskell

This section is a guide for the instructor to introduce Haskell syntax. Each of
these points should be covered before attempting the exercises.

* values, assignment
* type signatures `::` reads as *has the type*
  * The `->` in a type signature is *right-associative*
* functions are values
* functions take arguments
  * functions take *only one argument* but we approximate without spoken
    language
  * functions can be declared inline using *lambda expressions*
  * the `\` symbol in a lambda expression denotes a Greek lambda
* operators, beginning with non-alpha character, are in infix position by
  default
  * use in prefix position by surrounding with *(parentheses)*
* regular identifiers, beginning with alpha character, are in prefix position by
  default
  * use in infix position by surrounding with ``backticks``
* polymorphism
  * type variables *always* start with a lower-case character
* data types, declared using the `data` keyword
  * following the `data` keyword is the *data type name*
  * following the data type name are zero of more type variables
  * then `=` sign
  * data types have zero or more constructors
    * data type constructors start with an upper-case character, or colon `(:)`
  * following each constructor is a list of zero or more *constructor arguments*
  * between each constructor is a pipe symbol `(|)`
  * the `deriving` keyword gives us default implementations for some functions
    on that data type
  * when constructors appear on the left side of `=` we are *pattern-matching*
  * when constructors appear on the right side of `=` we are *constructing*
* type-classes

### Learning the tools

When this course is run in-person, some tools, particularly within Haskell, are
covered first.

* GHCi
  * `:type`
  * `:info`
* values
* type signatures
  * `x :: T` is read as *x is of the type T*
* functions are values
* functions take arguments
* functions take one argument
* lambda expressions
* operators (infix/prefix)
  * identifiers starting with `isAlpha` are prefix by default, infix surrounded in backticks (\`)
  * other identifiers are infix by default, prefix surrounded in parentheses
* data types
  * `data` keyword
  * recursive data types
* pattern matching
* `deriving` keyword
* type-classes
* type parameters
  * always lower-case 'a'..'z'
  * aka generics, templates C++, parametric polymorphism
* running the tests
  * `cabal test`

### Parser grammar assistance

The exercises in `Parser.hs` can be assisted by stating problems in a specific way, with a conversion to code.

| English   | Parser library                    |
|-----------|-----------------------------------|
| and then  | `bindParser` `>>=`                |
| always    | `valueParser` `pure`              |
| or        | `\|\|\|`                             |
| 0 or many | `list`                            |
| 1 or many | `list1`                           |
| is        | `is`                              |
| exactly n | `thisMany n`                      |
| fail      | `failed`                          |
| call it x | `\x ->`                           |

### Monad comprehension

##### do-notation

* insert the word `do`
* turn `>>=` into `<-`
* delete `->`
* delete `\`
* swap each side of `<-`

##### LINQ

* write `from` on each line
* turn `>>=` into in
* delete `->`
* delete `\`
* swap each side of `in`
* turn value into `select`

### Demonstrate IO maintains referential transparency

Are these two programs, the same program?

    p1 ::
      IO ()
    p1 =
      let file = "/tmp/file"
      in  do  _ <- writeFile file "abcdef"
              x <- readFile file
              _ <- putStrLn x
              _ <- writeFile file "ghijkl"
              y <- readFile file
              putStrLn (show (x, y))

    p2 ::
      IO ()
    p2 =
      let file = "/tmp/file"
          expr = readFile file
      in  do  _ <- writeFile file "abcdef"
              x <- expr
              _ <- putStrLn x
              _ <- writeFile file "ghijkl"
              y <- expr
              putStrLn (show (x, y))

What about these two programs?

    def writeFile(filename, contents):
        with open(filename, "w") as f:
        f.write(contents)

    def readFile(filename):
        contents = ""
        with open(filename, "r") as f:
        contents = f.read()
        return contents

    def p1():
        file = "/tmp/file"

        writeFile(file, "abcdef")
        x = readFile(file)
        print(x)
        writeFile(file, "ghijkl")
        y = readFile(file)
        print (x + y)

    def p2():
        file = "/tmp/file"
        expr = readFile(file)

        writeFile(file, "abcdef")
        x = expr
        print(x)
        writeFile(file, "ghijkl")
        y = expr
        print (x + y)

### One-day

Sometimes this course material is condensed into one-day. In these cases, the
following exercises are recommended:

* `Optional`
  * `mapOptional`
  * `bindOptional`
  * `(??)`
  * `(<+>)`
* `List`
  * `headOr`
  * `product`
  * `length`
  * `map`
  * `filter`
  * `(++)`
  * `flatMap`
  * `reverse`
* `Functor`
  * `instance Functor List`
  * `instance Functor Optional`
  * `instance Functor ((->) t)`
  * `instance Functor void`
* `Applicative`
  * `instance Applicative List`
  * `instance Applicative Optional`
  * `instance Applicative ((->) t)`
  * `lift2`
  * `sequence`
* `FileIO`

## Practice

[State of the Haskell ecosystem](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md) gives a general overview. Also see [Haskell survey 2018](https://www.fpcomplete.com/blog/2018-haskell-survey-results).

### Performance

- https://www.microsoft.com/en-us/research/wp-content/uploads/2016/07/haskell-beats-C.pdf (entirely not for distribution)
- https://www.stackbuilders.com/tutorials/haskell/ghc-optimization-and-fusion
- https://www.fpcomplete.com/blog/2014/08/iap-speeding-up-conduit (also see https://github.com/snoyberg/conduit)
- https://www.fpcomplete.com/blog/2017/07/iterators-streams-rust-haskell
- https://github.com/nh2/haskell-cpu-instruction-counter
- https://haskell-lang.org/tutorial/primitive-haskell

The reference literature here is the [GHC user guide](https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/).

A special shoutout for the **[Parallel and Concurrent Programming in Haskell](https://simonmar.github.io/pages/pcph.html)** book (available online [here](https://www.oreilly.com/library/view/parallel-and-concurrent/9781449335939/)).

### DSLs for embedded

#### Clash

- https://clash-lang.org/
- https://clash-lang.org/blog/0001-matrix-multiplication/

#### [Ivory](https://www.ivorylang.org/)

### Dynamic front-end

With Haskell:

- https://github.com/dmjio/miso

Or with Purescript:

- https://leanpub.com/purescript/
- http://try.purescript.org/

### Kafka

https://hackage.haskell.org/package/hw-kafka-client (also [milena](https://github.com/adamflott/milena), for a lens API).

### Data analysis

See SOTU for an overview, http://www.datahaskell.org/docs/community/current-environment.html for an assortment of links, and I'd like to hightlight:

- https://blog.jle.im/entry/introducing-the-backprop-library.html

### Idiomatic design

- https://hackage.haskell.org/package/safe-money
- https://diogocastro.com/blog/2018/10/17/haskells-kind-system-a-primer/

- https://blog.functorial.com/posts/2015-12-06-Counterexamples.html
- https://bartoszmilewski.com/2014/01/14/functors-are-containers/

- https://github.com/hmemcpy/milewski-ctfp-pdf

- https://lexi-lambda.github.io/blog/2018/02/10/an-opinionated-guide-to-haskell-in-2018/
- <https://downloads.haskell.org/~ghc/latest/docs/html/users_guide/lang.html>

### Telegram bots

Either https://www.stackage.org/package/telegram-bot-simple with a TEA design, or <https://github.com/klappvisor/haskell-telegram-api>.

## Literature

### Intro

Read <http://haskellbook.com> and do small projects while you're at it. Links above and below should help you with these.

### Intermediate

- Parallel and Concurrent Programming in Haskell
- <https://joyofhaskell.com/>
- <https://typeclasses.com/> (courses/posts, not a book)
- <https://intermediatehaskell.com/>

### Theoretical background

- See the [Idiomatic design](#idiomatic-design) section above.
- Type-Driven Development in Idris
- Confusion in the Church-Turing Thesis
- Anything Simon Marlow or Philip Wadler or Simon Peyton Jones write (from the very top of my head; non-exhaustive)

### Verification

- Software Foundations
- Programming Language Foundations in Agda
- <http://iacsv.kocherga.club>
